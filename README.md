This is a lightweight class for communicating with the MiLight/LimitlessLED/AppLamp WiFi box.

## Usage
```javascript
const { MiBoxConnection } = require("milight-wifi-box");

//create the connection
const ip = "192.168.1.5";
const port = 5987; //optional
let miConnection = new MiBoxConnection(ip, port);

let group = 1;
let lightType = "rgbww"; //can be "rgbww", "rgbw", or anything else for the default
miConnection.sendOnCmd(lightType, group);
miConnection.sendBrightnessCmd(100, lightType, group); //brightness can be between 0-100
miConnection.sendHueCmd(255, lightType, group); //hue can be between 0-255
miConnection.sendSaturationCmd(100, lightType, group); //saturation can be between 0-100
miConnection.sendKelvinCmd(100, lightType, group); //for rgbww lights, kelvin can be between 0-100
miConnection.sendOffCmd(lightType, group);

//the group and lightType arguments are optional
//you can also change the default group and lightType like this
miConnection.defaultLightType = "rgbw"; //default is "rgbww"
miConnection.defaultGroup = 1; //default is 0
miConnection.sendOnCmd();
miConnection.sendBrightnessCmd(100);

//or additionally you can set the rgb of a light,
//this will automatically be converted to the right commands using hsv
miConnection.setRgb(255, 255, 0);
```